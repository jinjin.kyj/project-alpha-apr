from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import CreateTask
from .models import Task


# Create your views here.
@login_required
def create_task(request):
    form = CreateTask(request.POST)
    if form.is_valid():
        task = form.save()
        task.owner = request.user
        task.save()
        return redirect("list_projects")

    else:
        form = CreateTask()

    context = {
        "form": form,
    }

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/mine.html", {"show_my_tasks": show_my_tasks})

from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from .forms import CreateProject


# Create your views here.
@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    context = {
        "show_project": show_project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    form = CreateProject(request.POST or None)
    if form.is_valid():
        task = form.save(commit=False)
        task.owner = request.user
        task.save()
        return redirect("list_projects")

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
